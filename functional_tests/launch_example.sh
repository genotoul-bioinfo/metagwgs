#!/bin/bash

module load bioinfo/Nextflow-v21.04.1
module load system/singularity-3.7.3
nextflow run -profile functional_test $METAG_PATH/main.nf  --min_contigs_cpm 1000 -with-report -with-timeline -with-trace -with-dag
