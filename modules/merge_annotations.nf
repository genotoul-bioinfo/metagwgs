process MERGE_ANNOTATIONS {
  publishDir "${params.outdir}/04_structural_annot/${meta.id}/", mode: 'copy'
  tag "${meta.id}"

  input:
    tuple val(meta), file(assembly_file), file(faa_file), file(cds_gff), file(rrna_gff), file(trna_gff)

  output:
    tuple val(meta), file("${meta.id}.gff"), emit: gff
    tuple val(meta), file("${meta.id}.ffn"), emit: ffn
    tuple val(meta), file("${meta.id}.faa"), emit: faa
    path "${meta.id}.txt", emit: report

  script:
  """
    merge_annotations.py -c $cds_gff -r $rrna_gff -t $trna_gff -v  \
                         --contig_seq $assembly_file --faa_file $faa_file \
                         --ffn_output ${meta.id}.ffn  --gff_output ${meta.id}.gff --faa_output ${meta.id}.faa \
                         --report_output ${meta.id}.txt
                
    replace_first_amino_acid.py ${meta.id}.ffn ${meta.id}.faa ${meta.id}Temp.faa

    awk '/^>/ {if (NR>1) printf("\\n"); printf("%s\\n",\$0); next; } { printf("%s",\$0);} END {printf("\\n");}' ${meta.id}Temp.faa > ${meta.id}Temp2.faa

    mv ${meta.id}.faa ${meta.id}_old.faa
    mv ${meta.id}Temp2.faa ${meta.id}.faa
  """
}
