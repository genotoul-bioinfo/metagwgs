#!/usr/bin/env python

"""--------------------------------------------------------------------
  Script Name: split_rna_clusters.py
  Description: Separate the rrna, trna and cds lines into 3 different
               files.
  Input file:  1st input file: featureCounts.tsv (output featureCount 
               tsv table)
  Output files:3 featureCounts.tsv files (rrna, trna, cds)
  Created By:  Philippe Ruiz
  Date:        2024-06-21
-----------------------------------------------------------------------
"""

# Metadata.
__author__ = 'Philippe Ruiz \
- Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2019 INRA'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'

# Modules importation.
from argparse import ArgumentParser
import pandas as pd

# Manage parameters.
def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description='Separate the rrna, trna and cds lines into 3 different files')

    parser.add_argument('-f', '--file_of_counts', required = True,
                        help = 'Table of counts for each gene from featureCounts.')
    
    args = parser.parse_args()
    return args

### Functions
def split_rna_cluster(file_of_counts):
    # Read count table
    df = pd.read_csv(file_of_counts)
    # Find rna lines
    condition = df.iloc[:, 0].astype(str).str.contains('tRNA|rRNA')
    cds = df[-condition]
    rna = df[condition]
    
    conditionT = rna.iloc[:, 0].astype(str).str.contains('tRNA')
    rrna = rna[-conditionT]
    trna = rna[conditionT]

    with open(file_of_counts, 'r') as f:
        headers = [next(f) for _ in range(2)]

    # Output path + file name
    cds_name = "cds." + file_of_counts
    rrna_name = "rrna." + file_of_counts
    trna_name = "trna." + file_of_counts

    with open(cds_name, 'w') as f_cds, open(rrna_name, 'w') as f_rrna, open(trna_name, 'w') as f_trna:
        f_cds.writelines(headers)
        f_rrna.writelines(headers)
        f_trna.writelines(headers)

    # Write splitted tables
    cds.to_csv(cds_name, mode='a', index=False, header=False)
    rrna.to_csv(rrna_name, mode='a', index=False, header=False)
    trna.to_csv(trna_name, mode='a', index=False, header=False)

    # Remove duplicated first row in cds table
    with open(cds_name, 'r') as f:
        lines = f.readlines()
    
    if len(lines) > 1:
        del lines[1]

    with open(cds_name, 'w') as f:
        f.writelines(lines)

def main():
    args = parse_arguments()
    split_rna_cluster(args.file_of_counts)

if __name__ == "__main__":
    main()