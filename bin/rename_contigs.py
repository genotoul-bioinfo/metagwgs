#!/usr/bin/env python3

"""
Rename assembly contigs.

Rename contig as <sample_name>_c<contig_number>

:Example:
rename_contigs.py -h
"""

# Metadata
__author__ = 'Mainguy Jean - Plateforme bioinformatique Toulouse'
__copyright__ = 'Copyright (C) 2020 INRAE'
__license__ = 'GNU General Public License'
__version__ = '0.1'
__email__ = 'support.bioinfo.genotoul@inra.fr'
__status__ = 'dev'


from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter, FileType
import logging
import gzip
import csv
from collections import defaultdict
import pyfastx
from Bio.Seq import Seq


def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)

    parser.add_argument('-s', '--sample', help='Sample name used to rename contigs.', required=True)
    
    parser.add_argument('-i', '--fna_file', help='Original fasta file of contigs.', required=True)

    parser.add_argument('-o', '--out_fna', help='Output fasta file with renamed contigs.', required=True)
    
    parser.add_argument('-t', '--contig_names_table', help='Tabular table with 2 fields : orignal and new name.', default="original_to_new_contig_name.tsv")

    parser.add_argument("-v", "--verbose", help="increase output verbosity",
                        action="store_true")

    args = parser.parse_args()
    return args



def main():

    args = parse_arguments()

    if args.verbose:
        logging.basicConfig(format="%(levelname)s: %(message)s", level=logging.DEBUG)
        logging.info('Mode verbose ON')

    else:
        logging.basicConfig(format="%(levelname)s: %(message)s")

    sample = args.sample    
    fna_file = args.fna_file
    out_fna = args.out_fna


    logging.info(f'Writting renamed fasta file in {out_fna}')
    with open(out_fna, "w") as fh_fna:
        for i, (name, seq) in enumerate(pyfastx.Fasta(fna_file, build_index=False)):
            fh_fna.write(f'>{sample}_c{i+1} {name}\n{seq}\n')

    sample = args.sample    
    fna_file = args.fna_file
    out_fna = args.out_fna
    old2new_contig_name = args.contig_names_table


    logging.info(f'Writting renamed fasta file in {out_fna}')
    with open(out_fna, "w") as fh_fna, open(old2new_contig_name, 'w') as mout:
        for i, (name, seq) in enumerate(pyfastx.Fasta(fna_file, build_index=False)):
            fh_fna.write(f'>{sample}_c{i+1} {name}\n{seq}\n')
            mout.write(f'{name}\t{sample}_c{i+1}\n')

if __name__ == '__main__':
    main()