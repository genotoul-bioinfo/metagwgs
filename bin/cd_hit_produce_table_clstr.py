#!/usr/bin/env python
import sys, re
from argparse import ArgumentParser, ArgumentDefaultsHelpFormatter

def parse_arguments():
    """Parse script arguments."""
    parser = ArgumentParser(description="...",
                            formatter_class=ArgumentDefaultsHelpFormatter)
    parser.add_argument('-i', '--input_file', required=True, help="cd-hit output file of representating clusters.")

    parser.add_argument('-o', '--output_file', required=True, help="Clusters table.")

    args = parser.parse_args()
    return args

def process(input_file, output_file):
    #init dictionaries:
    ref = ""
    seqs = []
    
    FH_out = open(output_file, 'wt')
    FH_input = open(input_file)
    for line in FH_input:
        #print line
        if line == '' :
            break
        else :
            if line[0] == ">":
                for seq in seqs :
                    FH_out.write(ref+"\t"+seq+"\n")
                ref = ""
                seqs = []
            else:
                a, b = line.split('>', 1)
                name = b.split("...")[0]
                rep = (line.rstrip()[-1] == '*')
                if rep :
                    ref = name
                    seqs.append(name)
                else :
                    seqs.append(name)

    for seq in seqs :
        FH_out.write(ref+"\t"+seq+"\n")

def main():
    args = parse_arguments()

    process(args.input_file, args.output_file)


if __name__ == '__main__':
    main()