/*
 * --------------------------------------------------------------------
 *  Nextflow base config file
 * --------------------------------------------------------------------
 */


process {


    // Process-specific resource requirements
    cpus = { 1 * task.attempt }
    memory = { 20.GB * task.attempt }

    errorStrategy = { task.exitStatus in [143,137,104,134,139,12] ? 'retry' : 'finish' }
    maxRetries = 3
    maxErrors = '-1'
    
    withName: CUTADAPT {
        cpus = 8
        memory = { 8.GB * task.attempt }
    }
    withName: SICKLE {
        memory = { 8.GB * task.attempt }
    }
    withLabel: FASTQC {
        cpus = 8
        memory = { 8.GB * task.attempt }
    }
    withName: MULTIQC {
        memory = { 8.GB * task.attempt }
    }
    withName: HOST_FILTER {
        memory = { 10.GB * task.attempt }
        time = '48h'
        cpus = 8
    }
    withName: INDEX_KAIJU {
        memory = { 200.GB * task.attempt }
        cpus = 6
    }
    withLabel: KAIJU {
        memory = { 50.GB * task.attempt }
        cpus = 25
    }
    withLabel: ASSEMBLY_SR {
        memory = { 110.GB * task.attempt }
        cpus = 10
    }
    withLabel: QUAST {
        cpus = 4
        memory = { 32.GB * task.attempt }
    }
    withName: READS_DEDUPLICATION {
        memory = { 128.GB * task.attempt }
    }
    withLabel: ASSEMBLY_FILTER {
        memory = { 2.GB * task.attempt }
        cpus = 1
    }
    withLabel: CD_HIT {
        memory = { 50.GB * task.attempt }
        cpus = 16
    }
    withName: QUANTIFICATION_TABLE {
        memory = { 2.GB * task.attempt }
    }
    withName: DIAMOND {
        cpus = 8
        memory = { 32.GB * task.attempt }
    }
    withName: GET_SOFTWARE_VERSIONS {
        memory = { 1.GB * task.attempt }
    }
    withName: EGGNOG_MAPPER_DB {
        cpus = 2
        memory = { 2.GB * task.attempt }
    }
    withName: EGGNOG_MAPPER {
        cpus = 4
        memory = { 60.GB * task.attempt }
    }
    withName: MERGE_QUANT_ANNOT_BEST {
        cpus = 1
        memory = { 50.GB * task.attempt }
    }
    withName: FUNCTIONAL_ANNOT_TABLE {
        cpus = 1
        memory = { 50.GB * task.attempt }
    }
    withName: ASSIGN_TAXONOMY {
        memory = { 5.GB * task.attempt }
        cpus = 1
    }
    withLabel: MINIMAP2 {
        memory = { 30.GB * task.attempt }
        cpus = 10
    }
    withLabel: ASSEMBLY_HIFI {
        memory = { 30.GB * task.attempt }
        cpus = 20
    }
    withName: BINETTE {
        memory = { 128.GB * task.attempt }
        cpus = 10
    }
    withName: GTDBTK {
        memory = { 64.GB * task.attempt }
        cpus = 16
    }
    withName: CHECKM2 {
        memory = { 20.GB * task.attempt }
        cpus = 20
    }
    withName: UNBINNED_CONTIGS {
        memory = { 1.GB * task.attempt }
        cpus = 1
    }
    withLabel: ADD_QUAST_INFO_TO_BINS {
        memory = { 2.GB * task.attempt }
        cpus = 1
    }
    withName: BINS_STATS_TO_MUTLIQC_FORMAT {
        memory = { 2.GB * task.attempt }
        cpus = 1
    }
    withName: GENOMES_ABUNDANCES_PER_SAMPLE {
        memory = { 2.GB * task.attempt }
        cpus = 1
    }
}
