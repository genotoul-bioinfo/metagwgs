# metagWGS: Usage

## I. Basic usage

* See [Installation](installation) to install metagWGS.

* See [Functional tests](functionnal_tests) to download the test dataset(s).

* Setup the samplesheet (example below or in `metagwgs-test-datasets/small/input/samplesheet.csv` in [metagwgs-test-datasets](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs-test-datasets)). See [dedicated section](#d-samplesheet) for more information.

* Run a basic script:

The next script is a script working on **genobioinfo slurm cluster**.
It allows to run the [step](use_case.md#step-s01_clean_qc) `S01_CLEAN_QC` of the pipeline (without host reads deletion and taxonomic affiliation of reads).

```{warning}
You must adapt it if you want to run it into your cluster. You must install/load Nextflow and Singularity, and define a specific configuration for your cluster.
```

* Create file named `Script.sh` with:

```bash
#!/bin/bash
#SBATCH -p workq
#SBATCH --mem=6G
module load bioinfo/Nextflow-v21.04.1
module load system/singularity-3.7.3   
nextflow run -profile test,genotoul metagwgs/main.nf \
   --type 'SR' \
   --input 'metagwgs-test-datasets/small/input/samplesheet.csv' \
   -skip_host_filter --skip_kaiju --stop_at_clean
```

```{note}
You can change Nextflow and Singularity versions with other versions available on the cluster (see all versions with `search_module ToolName`). Nextflow version must be >= v20 and Singularity version must be >= v3.
```

* Run `Script.sh` with this command line:

```bash
sbatch Script.sh
```

See the description of output files in [this part](output) of the documentation and [there](functionnal_tests#iv-output).

`Script.sh` is a basic script that requires only a small test data input and no other files.
To analyze real data, in addition to your metagenomic whole genome shotgun `.fastq/.fastq.gz` and/or assembly `.fa/.fasta` files,
 you need to download different files which are described in the next chapter.

```{warning}
If you run metagWGS to **analyze real metagenomics data on genobioinfo cluster**, you have to use the `unlimitq` 
queue to run your Nextflow script. To do this, instead of writing in the second line of your script `#SBATCH -p workq` 
you need to write `#SBATCH -p unlimitq`.
```

## II. Input files

```{warning}
Do not use any accent in any path or file name.
```

### A. General mandatory files

Launching metagWGS involves the use of mandatory files:

* The **metagenomic whole genome shotgun data** you want to analyze: `.fastq/.fastq.gz`
R1 and R2 files (Illumina HiSeq3000, NovaSeq sequencing, 2\*150bp).
For a cleaner MultiQC html report at the end of the pipeline, raw data with extensions `_R1` and `_R2`
are preferred to those with extensions `_1` and `_2`. Or single end PacBio HiFi reads `.fastq/.fastq.gz`.

* Or the **assemblies** you want to analyse: `.fa/.fasta` (if you have assembled your data outside metagWGS).

* The **metagWGS.sif** and **binning.sif** Singularity images (in `metagwgs/env` folder).

```{warning}
Make sure you have enough disk space: at least 20 times the volume occupied by the compressed input data (indicative volume).
```

### B. Mandatory files for certain steps

In addition to the general mandatory files, if you wish to launch certain steps of the pipeline, you will need previously generated or downloaded files:

* Step `S01_CLEAN_QC`, **only if you want to filter host reads**: you need a fasta file of the host genome.

* Step `S05_ALIGNMENT`, **(against a protein database)**: download the protein database you want to use. For example you can use NR database (in .dmnd format)

* Step `S08_BINNING`, To perform the taxonomic affiliations of the bins, you must download the gtdb-tk database as follows (see [GTDBTk](https://ecogenomics.github.io/GTDBTk/installing/index.html)) :

```bash
wget https://data.gtdb.ecogenomic.org/releases/latest/auxillary_files/gtdbtk_v2_data.tar.gz
wget https://data.ace.uq.edu.au/public/gtdb/data/releases/latest/auxillary_files/gtdbtk_v2_data.tar.gz  (or, mirror)
tar xvzf gtdbtk_v2_data.tar.gz
```

```{note}
metagWGS v2.4.3 use GTDB-Tk v2.1.1 and require the gtdb databank r207-v2 or r214.
```

After that, you have to indicate the path to gtdb-tk file database with the flag --gtdbtk_bank .

```{warning}
* if you use steps `S02_ASSEMBLY` or `S03_FILTERING` or `S04_STRUCTURAL_ANNOT` or `S05_ALIGNMENT` or `S06_FUNC_ANNOT` or
`S07_TAXO_AFFI` without skipping `S01_CLEAN_QC` or host reads filtering, you need to use the mandatory files of step `S01_CLEAN_QC`.

* if you use steps `S06_FUNC_ANNOT` or `S07_TAXO_AFFI`, you need to use the mandatory files of step `S05_ALIGNMENT`.
```

### C. Others files for certain steps

In addition to the `general mandatory files` and `mandatory files for certain steps`, if you wish to launch certain steps of the pipeline, 
you can download files/databases before running metagWGS. It is not mandatory but it avoids unnecessary downloads.

* Step `S01_CLEAN_QC`:

    * **Only if you want to filter host reads**: if you also have the BWA index (.amb, .ann, .bwt, .pac and .sa files)
    of the host genome fasta file, you can specify it as a metagWGS parameter: `--host_index`.

    * **Only if you want to have the taxonomic affiliation of reads**: you can previously download kaiju database index 
    `here <http://kaiju.binf.ku.dk/server>`_.
    (blue insert on the left side, click right of the desired database -> copy the link address).
    For example, download `refseq 2020-05-25 (17GB)` with
    `wget http://kaiju.binf.ku.dk/database/kaiju_db_refseq_2020-05-25.tgz`
    and unpack it with `tar -zxvf kaiju_db_refseq_2020-05-25.tgz`.
    These files are not mandatory, a metagWGS parameter allows to download automatically
    the wanted database among all available in the 
    [kaiju website](https://bioinformatics-centre.github.io/kaiju/downloads.html).

```{warning}
You are not authorized to use kaiju database built with `kaiju-makedb` command line.
```

### D. Samplesheet

The basic structure of the samplesheet is as follows:

```
sample,fastq_1,fastq_2
name_sample1,path_sample1_R1.fastq.gz,path_sample1_R2.fastq.gz
name_sample2,path_sample2_R1.fastq.gz,path_sample2_R2.fastq.gz
```

For HiFi the fastq2 column is not needed 

* **If you want to use external assemblies** : if you already have the assemblies for your data, 
you can directly specify the paths to the assemblies for each sample in the samplesheet (see example). 
The fastq cleaning and assembly filtering will be done automatically, to skip them, see the section [Skip](usage.md#skip). 

Samplesheet example :

```
sample,fastq_1,fastq_2,assembly
name_sample1,path_sample1_R1.fastq.gz,path_sample1_R2.fastq.gz,path_sample1_assembly.fasta
name_sample2,path_sample2_R1.fastq.gz,path_sample2_R2.fastq.gz,path_sample2_assembly.fasta
```

* **If you have the same sample sequenced on several flowcells**: the workflow will perform the cleaning step on the fastq files 
independently and merge the cleaned fastq before the assembly step. In order for the fastq to be merged per sample,
the samplesheet must have this form:

```
sample,flowcell,fastq_1,fastq_2
name_sample1,number_flowcell,path_sample1_flowcell1_R1.fastq.gz,path_sample1_flowcell1_R2.fastq.gz
name_sample1,number_flowcell,path_sample1_flowcell2_R1.fastq.gz,path_sample1_flowcell2_R2.fastq.gz
name_sample2,number_flowcell,path_sample2_R1.fastq.gz,path_sample2_R2.fastq.gz
```

* **If you want to perfom coassembly or cross alignment for binning on groups** : 

```
sample,group,fastq_1,fastq_2
name_sample1,group_name,path_sample1_flowcell1_R1.fastq.gz,path_sample1_flowcell1_R2.fastq.gz
name_sample2,group_name,path_sample1_flowcell2_R1.fastq.gz,path_sample1_flowcell2_R2.fastq.gz
name_sample3,group_name,path_sample2_R1.fastq.gz,path_sample2_R2.fastq.gz
```

```{warning}
Please, use full paths, not just file names in your samplesheet.
```

## III. Nextflow options

Analyzing your metagenomic data with metagWGS allows you to use all **`nextflow run` options** in your `nextflow run` 
command line and different **metagWGS specific parameters**. Some of these specific parameters are useful to indicate the `<PATH>` 
to these input files. The next chapters will explain these options and parameters.


```{note}
All `nextflow run` options available [here](https://www.nextflow.io/docs/latest/cli.html?highlight=trace#run) 
can be used when you run metagWGS. They are notified in the command line by `-`.
```

### A. Mandatory option

#### `-profile`

It allows you to choose the configuration profile among:
* `singularity` to analyze **your files** with metagWGS with **singularity containers**. 
You must have installed Singularity and downloaded the two Singularity containers associated to 
metagWGS (see [Installation page](installation.md#ii-install-metagwgs)). Thus, your results will be reproducible.

| Profile | Description |
| ------------ | ----------- |
| `genotoul` | to analyze **your files** with metagWGS **on genobioinfo cluster** with Singularity images `metagWGS.sif` and `binning.sif`.<br />This profile include the `singularity` profile.|
| `test` | to analyze **small test data files** (used in [I. Basic Usage](usage.md#i-basic-usage)) <br /> with metagWGS **on genobioinfo cluster** on the **`workq`** queue <br /> with Singularity images `metagWGS.sif` and `binning.sif`.|
| `local` | to launch the pipeline in local executor. |
| `debug` | to **debug** metagWGS pipeline. |

```{note}
There is no definition of the type of cluster (SGE, slurm, etc) you use in this profile.
```

These profiles are associated to different configuration files found [in this directory](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/tree/master/conf?ref_type=heads).
The `base.config` file available in this directory is the base configuration load in first which
is crushed by indications of the profile you use. See [here](https://genotoul-bioinfo.pages.mia.inra.fr/use-nextflow-nfcore-course/pages/nfcore_profiles/) for more explanations.

```{note}
Two or more configuration profiles can be specified by separating the profile names with a comma character, for example: `-profile genotoul,test`.
```

### B. Useful options

| Option | Description |
| ------ | ----------- |
| `-resume` | It allows to rerun metagWGS from the lastest process uncorrectly ended <br /> or from a process where input or output files have changed. |
| `-with-report` | Generates a `report.html` file describing the use of memory and cpus for each process. |
| `-with-timeline` | Generates a `timeline.html` file describing the duration of each process. |
| `-with-trace` | Generates a `trace.txt` file describing the location of cache directory and metrics for each process. |
| `-with-dag` | Generates a `dag.dot` file, a graph representing the pipeline. |
| `-w working_directory_name` | Allows to choose the name of the cache directory. Default `-w work`. |

## IV. metagWGS parameters

The next parameters can be used when you run metagWGS.

```{note}
The specific parameters of the pipeline are indicated by `--` in the command line.
```

### A. Mandatory parameters:

| Parameter    | Description |
| ------------ | ----------- |
| `--input "<PATH>/samplesheet.csv"` | indicate location of the samplesheet containing paths to the input reads `.fastq/.fastq.gz` and/or assembly `.fa/.fasta` files. <br /> For example, `--input "<PATH>/samplesheet.csv"` runs the pipeline with all the `R1.fastq.gz` and `R2.fastq.gz` files available in the indicated `<PATH>`. <br /> For a cleaner MultiQC html report at the end of the pipeline, raw data with extensions `_R1` and `_R2` are preferred to those with extensions `_1` and `_2`.|
|`--type "SR" or "HIFI"` | indicate the type of the sequencing data, for short-read Illumina data <br /> use `"SR"`, for long-read data PacBio HiFi use `"HIFI"`.|

### B. `--stop_at_[STEP]` and `--skip_[STEP]` parameters:

By default, all steps of metagWGS will be launched on the input data. From S01_CLEAN_QC to S08_BINNING.

#### 1. Stop at:

You can use a `stop_at_[STEP]` parameter to launch only the steps leading to and including the one you specified.

`--stop_at_[STEP]`: indicate the step of the pipeline you want to stop at. The steps available are described 
[here](#3-other-parameters-step-by-step) (`S01_CLEAN_QC`, `S02_ASSEMBLY`, `S03_FILTERING`, `S04_STRUCTURAL_ANNOT`).

```{note}
`S05_ALIGNMENT`, `S06_FUNC_ANNOT`, `S07_TAXO_AFFI` and `S08_BINNING` being the 4 last steps, there is no
`--stop_at_[STEP]`; see 'Skip' subsection for more information.
```

For each [STEP](#3-other-parameters-step-by-step), specific parameters are available. You can add them to the command line and run the pipeline.
They are described in the section [other parameters step by step](#3-other-parameters-step-by-step).

#### 2. Skip:

In parallel, you can use any combination of `--skip_[STEP]` parameters you want to skip certain steps:

| Parameter     | Description |
| -------------  | ----------- |
| `--skip_clean`  | skip the `S01_CLEAN_QC` step entirely, beginning the pipeline at step `S02_ASSEMBLY`|
| `--skip_filtering`| skip the `S03_FILTERING` step entirely, continuing the pipeline at step `S04_STRUCTURAL_ANNOT`|
| `--skip_func_annot`| skip the `S06_FUNC_ANNOT` step entirely, ending the pipeline at step `S05_ALIGNMENT` or `S07_TAXO_AFFI` or `S08_BINNING`|
| `--skip_taxo_affi`| skip the `S07_TAXO_AFFI` step entirely, ending the pipeline at step `S05_ALIGNMENT` or `S06_FUNC_ANNOT` or `S08_BINNING`|
| `--skip_binning`| skip the `S08_BINNING` step entirely, ending the pipeline at step `S05_ALIGNMENT` or `S06_FUNC_ANNOT` or `S07_TAXO_AFFI`|

```{note}
Using each `--skip_func_annot` and `--skip_taxo_affi` and `--skip_binning` parameters will stop the pipeline at step `S05_ALIGNMENT`.
```

### C. Other parameters step by step

#### 1. `S01_CLEAN_QC` step:

There are 5 substeps in this step each of them with specific parameters:

- Remove adapter sequences and low quality reads with cutadapt

| Parameter | Description |
| ------------ | ----------- |
| `--adapter1 "adapter_sequence"` | nucleotidic sequence of read 1 adapter (cutadapt -a option).<br />Default `"AGATCGGAAGAGC"`: [GeT-PlaGe](https://ng6.toulouse.inra.fr/index.php?id=57) Illumina adapters.<br />This adapters depends on the library kit used before sequencing data. |
| `--adapter2 "adapter_sequence"` | nucleotidic sequence of read 2 adapter (cutadapt -A option).<br />Default `"AGATCGGAAGAGC"`: [GeT-PlaGe](https://ng6.toulouse.inra.fr/index.php?id=57) Illumina adapters.<br />This adapters depends on the library kit used before sequencing data. |

- Remove low quality reads with sickle

| Parameter | Description |
| ------------ | ----------- |
| `--use_sickle` | Use sickle process for high quality trimming.<br />Default: `true`. |
| `--quality_type` | "solexa" or "illumina" or "sanger": sickle -t quality type parameter.<br />Default: `sanger`. |

- Remove host reads with bwa, samtools and bedtoools

| Parameter | Description |
| ------------ | ----------- |
| `--skip_host_filter` | allows to skip the deletion of host reads.<br />Default: `false`. |
| `--host_fasta` | "$PATH/genome_name.fasta": indicate the nucleotide sequence of the host genome.<br /> Default: `""`.|
| `--host_index` | "$PATH/genome_name.{0123,amb,ann,bwt.2bit.64,pac,sa}": indicate the bwa-mem2 index files if they are already built/downloaded. <br /> Default: `""` corresponding to the building of bwa-mem2 index files by metagWGS.|

Depending on the size of your file, you may need to tweak
the memory and cpus settings of the Nextflow process to filter the host reads. If this is the case,
create a `nextflow.config` file in our working directory and modify these parameters, such as :

```bash
withName: HOST_FILTER {
   memory = { 200.GB * task.attempt }
   time = '48h'
   cpus = 8
}
```

```{warning}
You need to use either `--skip_host_filter` or `--host_fasta` or `--skip_clean`. If it is not the case, an error message will occur.
```

- Quality control of raw data and cleaned data with FastQC

No parameter available for this substep.

- Taxonomic classification of reads with kaiju

| Parameter | Description |
| ------------ | ----------- |
| `--kaiju_db_dir` | "<PATH>/directory": if you have already downloaded the kaiju database, indicate its directory.<br /> Default: `""`.|
| `--kaiju_db` | "http://kaiju.binf.ku.dk/database/CHOOSEN_DATABASE.tgz": allows metagWGS to download the kaiju database of your choice.<br /> The list of kaiju databases is available in `kaiju website <http://kaiju.binf.ku.dk/server>`, in the blue insert on the left side.<br /> Default: `--kaiju_db https://kaiju.binf.ku.dk/database/kaiju_db_refseq_2021-02-26.tgz`. See warning.<br />|
| `--skip_kaiju` | allows to skip taxonomic affiliation of reads with kaiju. Krona files will not be generated. Use: `--skip_kaiju`. See warning.|

```{warning}
You will not be able to use kaiju database built with `kaiju-makedb` command line. Default: `--kaiju_db_dir false`. See warning.

You need to use either `--kaiju_db_dir` or `--kaiju_db` or `--skip_kaiju`. If it is not the case, an error message will occur.
```

```{note}
For HiFi reads, adapters and low quality reads are not filtered. Host reads are removed using minimap2 that does not
require index files and therefore the `--host_index` parameter is not necessary.
```

#### 2. `S02_ASSEMBLY` step:

```{warning}
`S02_ASSEMBLY` step depends on `S01_CLEAN_QC` step. You need to use the mandatory files of these two steps to run `S02_ASSEMBLY`.
See [II. Input files](usage.md#ii-input-files) and warnings.
```

| Parameter | Description |
| ------------ | ----------- |
| `--assembly` | allows to indicate the assembly tool.<br /> For short reads: `["metaspades" or "megahit"]`:  Default: `metaspades`.<br /> For HiFi reads:  `["hifiasm-meta", "metaflye"]`. Default: `hifiasm-meta`.|
| `--coassembly` | allows to assemble together the samples labeled with the same group in the samplesheet. <br /> It will generate one assembly for each group. To co-assemble all of your samples together, <br /> you must indicate a unique group for each sample in the samplesheet.|

```{warning}
* With the coassembly, you can't use `--binning_cross_alignment 'group'`  because one binning will be generate 
for each group co-assembled and automatically mapping with every sample of his group but 
`--binning_cross_alignment 'all'` can be use to cross align every sample with every group. 

* For short reads, the user can choose between `metaspades` or `megahit` for `--assembly` parameter. 
The choice can be based on CPUs and memory availability: `metaspades` needs more CPUs and memory than `megahit` but
our tests showed that assembly metrics are better for `metaspades` than `megahit`. For PacBio HiFi reads, the user
can choose between `hifiasm-meta` or `metaflye`.
```

```{note}
You may need to tweak the memory and cpus settings of the Nextflow process, especially if you are using `metaspades`.
If this is the case, create a `nextflow.config` file in our working directory and modify these parameters
(be aware that the memory must be in GB) such as :

```bash
withName: ASSEMBLY {
   memory = { 440.GB * task.attempt }
   cpus = 20
}
```

```{note}
It is also possible to assemble HiFi reads with the tool HiCanu. If you want to use HiCanu
generate the assembly first and then launch the rest of the pipeline.
```


#### 3. `S03_FILTERING` step:

```{warning}
`S03_FILTERING` step depends on `S01_CLEAN_QC` and `S02_ASSEMBLY` steps. You need to the use mandatory
files of these three steps to run `S03_FILTERING`. See [II. Input files](usage.md#ii-input-files) and warnings.
```

| Parameter | Description |   
| ------------ | ----------- |
| `--min_contigs_cpm [cutoff_value]` | CPM (Count Per Million) cutoff to filter contigs with low number of reads.<br /> [cutoff_value] can be a decimal number (example: `0.5`). Default: `1`.|

#### 4. `S04_STRUCTURAL_ANNOT` step:

No parameters.

```{warning}
`S04_STRUCTURAL_ANNOT` step depends on `S01_CLEAN_QC`, `S02_ASSEMBLY` and `S03_FILTERING` steps (if you use it).
You need to use the mandatory files of these four steps to run `S04_STRUCTURAL_ANNOT`.
See [II. Input files](usage.md#ii-input-files) and warnings.
```

#### 5. `S05_ALIGNMENT` step:

```{warning}
`S05_ALIGNMENT` step depends on `S01_CLEAN_QC`, `S02_ASSEMBLY`, `S03_FILTERING` (if you use it) and `S04_STRUCTURAL_ANNOT` steps.
You need to use the mandatory files of these five steps to run `S05_ALIGNMENT`.
See [II. Input files](usage.md#ii-input-files) and warnings.
```

| Parameter | Description |
| ------------ | ----------- |
| `--diamond_bank` | "<PATH>/bank.dmnd": path to diamond bank used to align protein sequence of genes.<br /> This bank must be previously built with [diamond makedb](https://github.com/bbuchfink/diamond/wiki).<br /> Default: `""`.|

Make sure that the version of the tools corresponds to the version of the bank specified in the parameter.
For example check the version of software in the yaml file and the singularity recipe in env/ repository and be sure that you specified the appropriate databank version.

```{warning}
You need to use a NCBI reference to have functional links in the output file _Quantifications_and_functional_annotations.tsv_ of `S06_FUNC_ANNOT` step.
```

#### 6. `S06_FUNC_ANNOT` step:

```{warning}
`S06_FUNC_ANNOT` step depends on `S01_CLEAN_QC`, `S02_ASSEMBLY`, `S03_FILTERING` (if you use it),
`S04_STRUCTURAL_ANNOT` and `S05_ALIGNMENT` steps. You need to use the mandatory files of these six steps to run
`S06_FUNC_ANNOT`. See [II. Input files](usage.md#ii-input-files) and warnings.
```

| Parameter | Description |
| ------------ | ----------- |
| `--percentage_identity [number]` | corresponds to cd-hit -c option to indicate sequence percentage identity for <br /> clustering genes. Default: `0.95` corresponding to 95% of sequence identity. Use: `number` must be between 0 and 1, <br /> and use `.` when you want to use a float. |
| `--eggnog_mapper_db_download` | downloads eggNOG-mapper database. If you don't use this parameter, metagWGS doesn't download <br /> this database and you must use `--eggnog_mapper_db_dir`. Use: `--eggnog_mapper_db_download`. See warning. |
| `--eggnog_mapper_db_dir "<PATH>/database_directory/"` | indicates path to eggNOG-mapper database if you have already dowloaded it. <br /> If you run the `S06_FUNC_ANNOT` step in different metagenomics projects, <br /> downloading the eggNOG-mapper database only once before running metagWGS avoids you to multiply the storage <br /> of this database and thus keep free disk space. See warning. |

Make sure that the version of the tools corresponds to the version of the bank specified in the parameter.
For example check the version of software in the yaml file and the singularity recipe in env/ repository 
and be sure that you specified the appropriate databank version.

```{warning}
You need to use either `--eggnog_mapper_db_download` or `--eggnog_mapper_db_dir`.
If it is not the case, an error message will occur. Do not use both parameters ! Check all the config files (genotoul.config and nextflow.config in particular)
```

#### 7. `S07_TAXO_AFFI` step:

```{warning}
`S07_TAXO_AFFI` step depends on `S01_CLEAN_QC`, `S02_ASSEMBLY`, `S03_FILTERING` (if you use it),
`S04_STRUCTURAL_ANNOT` and `S05_ALIGNMENT` steps. You need to use the mandatory files of these six steps
to run `S07_TAXO_AFFI`. See [II. Input files](usage.md#ii-input-files) and warnings.
```

| Parameter | Description |
| ------------ | ----------- |
| `--accession2taxid` "<PATH>/prot.accession2taxid.FULL.gz" | indicates the local path or FTP adress of the NCBI file `prot.accession2taxid.FULL.gz`. <br /> Default: `"ftp://ftp.ncbi.nih.gov/pub/taxonomy/accession2taxid/prot.accession2taxid.FULL.gz"`. <br /> The local file can be gzip or not. |
| `--taxdump` "<PATH>/new_taxdump.tar.gz" | indicates the local path or the FTP adress of the NCBI file `taxdump.tar.gz`. <br /> Default: `"ftp://ftp.ncbi.nih.gov/pub/taxonomy/new_taxdump/new_taxdump.tar.gz"`. <br /> The local file can be either a tar.gz archive or the extracted `new_taxdump` directory. |

```{warning}
To have contigs and genes taxonomic affiliation your protein database used in the step 05_alignment has
to come from ncbi and your `taxdump` and `prot.accession2taxid` files must be coherent, i.e. downloaded 
at the same time as the protein database used in 05_alignment step.
```

#### 8. `S08_binning` step:

```{image} images/08_binning.png
:alt: binning
:class: bg-primary
:width: 100%
:align: center
```

| Parameter | Description |   
| ------------ | ----------- |
| `--gtdbtk_bank` | indicates path to the GTDBTK database (see [GTDBTk installing](https://ecogenomics.github.io/GTDBTk/installing/index.html)).<br /> Default: `""`.|
| `--checkm2_bank` | indicates path to the checkm2 database (`PATH/uniref100.KO.1.dmnd`).<br /> To download it, launch `singularity exec -B YOUR_ACTUAL_PATH -B /bank -B /work -B /save -B /home -B /work/project -B /usr/local/bioinfo PATH_TO/binning.sif checkm2 database --download --path YOUR_ACTUAL_PATH/checkm2DB`.<br /> Adapt -B parameter to your infrastructure if you are not in genobioinfo. This command will return 2 non-blocking errors, don't worry about them. <br /> Default: `""`.|
| `--metabat2_seed` | Set the seed for metabat2, for exact reproducibility of metabat2 (default: 0 (random seed)).<br /> Default: `0`.|
| `--min_completeness [nb]` | Minimum % of bins completeness for the bins to be kept after bin_refinement step.<br /> Default: `50`.|
| `--binning_cross_alignment ["all","group","individual"]` | defines mapping strategy to compute co-abundances for binning.<br /> `all` means that each samples will be mapped against every assembly, `group` means that all sample from a group will<br /> be mapped against every assembly of the group, `individual` means that each sample will only be mapped against his assembly.<br /> Default: `individual`.|
| `--drep_threshold [nb]` | Average Nucleotide Identity (ANI) threshold used for bins de-replication.<br /> Default: `0.95` corresponding to `95%`.<br /> Use a number between 0 and 1. Most studies agree that 95% ANI is an appropriate threshold for species-level de-replicationton.<br /> If the goal of dereplication is to generate a set of genomes that are distinct when mapping short reads, 98% (0.98) ANI is an appropriate threshold.<br /> Default: `0.95`.|

```{note}
metagWGS v2.4.3 use GTDB-Tk v2.1.1 and require the gtdb databank r207-v2 or r214.
```

```{warning}
* `CheckM2` has reproducibility issues, therefore this step is not perfectly reproducible from one run to another. Results may vary slightly. 

* `pplacer` used in `gtdb-tk` randomly unclassified genomes. If you have unclassified genomes, please rerun `gtdb-tk` by
using singularity shell (see in work directory that you can find from trace*.out file the file .command.run
the singularity exec command) and adapt the `gtdbtk` command in `.command.sh` to run it only on unclassified genomes.
Unclassified genomes are in `classify/gtdbtk.bac120.disappearing_genomes.tsv` file.
```

### D. Others parameters

| Parameter | Description |
| ------------ | ----------- |
| `--multiqc_config "<PATH>/multiqc.yaml"` | if you want to change the configuration of multiqc report. <br /> Default: `"$baseDir/assets/multiqc_config.yaml"`. |
| `--outdir "dir_name"` | change the name of output directory. Default `"results"`. |
| `--databases "dir_name"` | change the location where databases will be downloaded if not provided by the user. <br /> Default `"databases"` in working directory. |
| `--help` | print metagWGS help. Default: `false`. |

### E. Check your configuration and parameters

Because it has many levels of configuration we advise you to check your configuration using the following command: 
For example:

```
module load bioinfo/Nextflow-v21.04.1; module load system/singularity-3.7.3; nextflow config -profile genotoul path_to/metagwgs/main.nf
```

will output all the configuration that will be used by the workflow.

## V. Description of output files

See the description of output files in [this part](output) of the documentation.

## VI. Analyze big test dataset with metagWGS in genobioinfo cluster

If you have an account on the [genobioinfo cluster](http://bioinfo.genotoul.fr/) and 
you would like to familiarize yourself with metagWGS, see the tutorial available in the
[use case documentation page](use_case). It allows the analysis of big test datasets with metagWGS.
(WARNING: use_case documentation is not up-to-date, needs to be updated)

```{warning}
The test dataset in `metagwgs-test-datasets/small` used in [I. Basic Usage](usage.md#i-basic-usage)
is a small test dataset which allows to test all steps but with few CPUs and memory.
```
