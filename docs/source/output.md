# metagWGS: Output

This document describes the output files produced by metagWGS.

## I. Description of output files and directories

The pipeline will create the following folders in your working directory:

| Directory| Description                                                                                                           |
|----------|-----------------------------------------------------------------------------------------------------------------------|
| work/    | Directory containing the Nextflow working files. <br /> Directory name can be changed if you use -w option of Nextflow. <br /> Once you are happy with the analysis and you do not intend to <br /> resume the pipeline execution, you can delete this directory. |
| results/ | Directory containing metagWGS output files. <br /> Directory name can be changed if you use –outdir parameter of metagWGS.                                                     |

## II. Description of output files in `results/` directory:

The `results/` directory contains a sub-directory for each step launched:

### A. 01_clean_qc

#### 1. 01_1_cleaned_reads

| File or directory       | Description                             |
| ----------------------- | --------------------------------------- |
| `cleaned_SAMPLE_NAME_R{1,2}.fastq.gz`        | There are one R1 and one R2 file for each sample. |
| `logs/`                                      | Contains cutadapt (`SAMPLE_NAME_cutadapt.log`) and sickle (`SAMPLE_NAME_sickle.log`) <br /> log files for each sample. |
| `SAMPLE_NAME_cleaned_R{1,2}.nb_bases`        | Only if you remove host reads, you have the number of nucleotides into each cleaned <br /> R1 and R2 files of each sample. |
| `SAMPLE_NAME.no_filter.flagstat`              | Only if you remove host reads, you also have a samtools flagstat file for each sample <br /> before removing host reads. |
| `host_filter_flagstat/`                      | There are the samtools flagstat files (`SAMPLE_NAME.host_filter.flagstat`) after removing host reads. |
| `SAMPLE_NAME.host_filter.flagstat`            | flagstat files after removing host reads. |

#### 2. 01_2_qc

| Directory       | Description                             |
| ----------------------- | --------------------------------------- |
| `fastqc_raw/SAMPLE_NAME/`       | Contains the results of fastQC on raw data: two files (`.html` and `.zip`) for <br /> R1 and two files for R2 (`.html` and `.zip`). |
| `fastqc_cleaned/cleaned_SAMPLE_NAME/`   | Same as `01_2_qc/fastqc_raw/` but for cleaned data. |

#### 3. 01_3_taxonomic_affiliation_reads

| File      | Description                             |
| ----------------------- | --------------------------------------- |
| `kaiju.krona.html`        | Krona file representing number of reads by taxonomical level. |
| `taxo_affi_reads_[taxo_level].txt`        | One file for each taxonomy level (phylum, order, class, family, genus, species).<br /> The first column corresponds to the name of the taxon, the second to the taxon id <br /> and then for each file there are 2 columns: the percentage of reads affiliated to this taxon <br /> and the number of reads affiliated to this taxon. |
| `SAMPLE_NAME_MEM_verbose_only_classified.out.gz`    | Compressed kaiju results. Each row corresponds to a classified reading. <br /> The first column 'C' indicates that the read is classified, the second column is the name of the read, the third is the NCBI taxon ID <br /> of the assigned taxon, the fourth is the length or score of the best match used for classification, <br /> the fifth is the taxon ID of all database sequences with the best match, the sixth is the accession number <br /> of all database sequences with the best match, and the last is the corresponding fragment sequence(s). |
| `match_length_kaiju_distribution.html`   |  Density plot of the size of the matches found by kaiju in reads of each sample. |

### B. 02_assembly

#### 1. 02_1_primary_assembly

```{note}
In this directory you have either the results of assembly with `metaspades` or `megahit`
if you analyse short read data and `hifiasm-meta` or `metaflye`.
if you analyse HiFi data. You have chosen your assembly tool with `--assembly` parameter.
```

| File or directory             | Description                                                                                                                      |
|--------------------------------|----------------------------------------------------------------------------------------------------------------------------------|
| `SAMPLE_NAME.fna`                             | unfiltered assembly (primary assembly) with renamed contigs. Contig names follow this pattern: <br /> `<sample_name>_c<contig_number>`. |
  | `assembly_metric`                             | Contains metaQUAST quality control files of contigs.    |
| `<assembly_tool>_SAMPLE_NAME/`                | output files of the assembly tool. It contains logs and original fasta files of the assembly before renaming.                    |
| `SAMPLE_NAME_original_to_new_contig_name.tsv` | Tabular file with two fields: original contig name from the assembly tool <br /> and the new contig name given by the pipeline.                   |
| `circular_contigs/`                           | only for HiFi: contains the fasta file of each circular contigs, useful for bin refinement in the binning step. |

#### 2. 02_2_deduplicated_reads

```{note}
This directory contains deduplicated reads. It is created only for short read data.
```

| File or directory                  | Description                                                  |
|-------------------------------------|--------------------------------------------------------------|
| `SAMPLE_NAME_R{1,2}_dedup.fastq.gz` | Deduplicated reads (R1 and R2 files) for SAMPLE_NAME sample. |

#### 3. 02_3_reads_vs_primary_assembly

```{note}
In this directory you have alignement metrics of reads aligned to the primary assembly
(before the asssembly filering step if any). For short reads, reads have been deduplicated.
```

| File or directory                     | Description                                                                                     |
|----------------------------------------|-------------------------------------------------------------------------------------------------|
| `SAMPLE_NAME/SAMPLE_NAME.bam`          | Samtools BAM file of sample reads align to the sample assembly.                                 |
| `SAMPLE_NAME/SAMPLE_NAME.coverage.tsv` | Samtools coverage file (see details [here](http://www.htslib.org/doc/samtools-coverage.html))   |
| `SAMPLE_NAME/SAMPLE_NAME.flagstat`     | Samtools flagstat file (see details  [here](http://www.htslib.org/doc/samtools-flagstat.html) ) |
| `SAMPLE_NAME/SAMPLE_NAME.idxstats`     | Samtools idxstat file (see details  [here](http://www.htslib.org/doc/samtools-flagstat.html) )  |


### C. 03_filtering

```{note}
This directory is created when the assembly filtering step is applied. 
```

#### 1. filtering_at_[cpm_threshold]cpm/03_1_filtered_assembly

| File or directory                                                     | Description                                                                                                  |
|------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------|
| `SAMPLE_NAME_select_contigs_cpm[cpm_threshold].fasta`                  | Selected contigs after filtering. This is the assembly fasta file used in the rest <br /> of the pipeline analysis. |
| `discard_contigs/SAMPLE_NAME_discard_contigs_cpm[cpm_threshold].fasta` | Contigs that have been removed from the assembly.                                                            |
| `assembly_metric/`                                                     | Contains metaQUAST quality control files of filtered assemblies. |

#### 2. filtering_at_[cpm_threshold]cpm/03_2_reads_vs_filtered_assembly

```{note}
This directory contains alignement metrics of reads aligned to the filtered assembly. 
When the filtering step has no effect on the assembly (no contig are filtered out), the bam of the 
reads over the assembly does not change with the one from the assembly step `02_assembly/02_3_reads_vs_primary_assembly` 
consequently the bam found here will be a symbolic link with the bam from the assembly file.
```

| File or directory                     | Description                                                                                     |
|----------------------------------------|-------------------------------------------------------------------------------------------------|
| `SAMPLE_NAME/SAMPLE_NAME.bam`          | Samtools BAM file of sample reads align to the sample assembly.                                 |
| `SAMPLE_NAME/SAMPLE_NAME.coverage.tsv` | Samtools coverage file (see details [here](http://www.htslib.org/doc/samtools-coverage.html))   |
| `SAMPLE_NAME/SAMPLE_NAME.flagstat`     | Samtools flagstat file (see details  [here](http://www.htslib.org/doc/samtools-flagstat.html) ) |
| `SAMPLE_NAME/SAMPLE_NAME.idxstats`     | Samtools idxstat file (see details  [here](http://www.htslib.org/doc/samtools-flagstat.html) )  |

### D. 04_structural_annot

| File                          | Description                                                      |
|-------------------------------|------------------------------------------------------------------|
| `SAMPLE_NAME/SAMPLE_NAME.faa` | Fasta file of protein sequences of structural annotated genes.   |
| `SAMPLE_NAME/SAMPLE_NAME.ffn` | Fasta file of nucleotide sequence of structural annotated genes. |
| `SAMPLE_NAME/SAMPLE_NAME.gff` | Coordinates of structural annotated genes into contigs.          |
| `SAMPLE_NAME/SAMPLE_NAME.txt` | Summary of the assembly structural annotation.                   |

### E. 05_protein_alignment

#### 1. 05_1_database_alignment

| File or directory             | Description                             |
| ----------------------- | --------------------------------------- |
| `SAMPLE_NAME/SAMPLE_NAME_aln_diamond.m8` | Diamond results file. |

### F. 06_func_annot

#### 1. 06_1_clustering

| File      | Description                             |
| ----------------------- | --------------------------------------- |
| `SAMPLE_NAME.cd-hit.[cd-hit percentage identity].fasta`                     | Amino acid sequences of representatives genes ("intra-sample clusters") <br /> generated by cd-hit with [cd-hit percentage identity] percentage identity. |
| `SAMPLE_NAME.cd-hit.[cd-hit percentage identity].fasta.clstr`               | Text file of list of intra-sample clusters generated by cd-hit with <br /> [cd-hit percentage identity] percentage identity. |
| `SAMPLE_NAME.cd-hit.[cd-hit percentage identity].table_cluster_contigs.txt` | Correspondance table of intra-sample clusters and initial genes. <br /> One line = one correspondance between an intra-sample cluster (first column) and an initial gene (second column). |
| `All-cd-hit.[cd-hit percentage identity].fasta`                             | Amino acid sequences of global representatives genes ("inter-sample clusters") <br /> generated by cd-hit with [cd-hit percentage identity] percentage identity. |
| `All-cd-hit.[cd-hit percentage identity].fasta.clstr`                       | Text file of list of inter-sample clusters generated by cd-hit with <br /> [cd-hit percentage identity] percentage identity. |
| `table_clstr.txt`                                                               | Correspondance table of inter-sample clusters and intra-sample clusters. <br /> One line = one correspondance between an inter-sample cluster (first column) and an intra-sample cluster (second column). |

#### 2. 06_2_quantification

| File      | Description                             |
| ----------------------- | --------------------------------------- |
| `SAMPLE_NAME.featureCounts.tsv.summary`     | featureCounts statistics by sample. |
| `SAMPLE_NAME.featureCounts.stdout`          | featureCounts log file by sample. |
| `SAMPLE_NAME.featureCounts.tsv`             | featureCounts output file by sample (cds only). |
| `all.SAMPLE_NAME.featureCounts.tsv`             | featureCounts output file by sample.|
| `rrna.SAMPLE_NAME.featureCounts.tsv`             | featureCounts output file by sample (rRNA only).|
| `trna.SAMPLE_NAME.featureCounts.tsv`             | featureCounts output file by sample (tRNA only).|

#### 3. 06_3_functional_annotation

| File      | Description                             |
| ----------------------- | --------------------------------------- |
| `SAMPLE_NAME_diamond_one2one.emapper.seed_orthologs` | eggNOG-mapper intermediate file containing seed matches onto eggNOG database. |
| `SAMPLE_NAME_diamond_one2one.emapper.annotations`    | eggNOG-mapper final file containing functional annotations for genes with <br /> a matches into eggNOG database. |
| `SAMPLE_NAME.best_hit`                               | Diamond best hits results for each gene. Best hits are diamond hits with <br /> the maximum bitScore for this gene. |
| `Quantifications_and_functional_annotations.tsv`     | Table where a row corresponds to an inter-sample cluster. Columns corresponds to quantification <br /> of the sum of aligned reads on all genes of each inter-sample cluster (columns `*featureCounts.tsv`), sum of abundance in all samples (column `sum`), <br /> eggNOG-mapper results (from `seed_eggNOG_ortholog` to `PFAMs` column) and diamond best hits results (last two columns `sseqid` and `stitle` <br /> correspond to `diamond_db_id`and `diamond_db_description`). |
| `GOs_abundance.tsv`                                  | Quantification table storing for each GO term (rows) the sum of aligned reads into all genes <br /> having this functional annotation for each sample (columns). |
| `KEGG_ko_abundance.tsv`                              | Quantification table storing for each KEGG_ko (rows) the sum of aligned reads into all genes <br /> having this functional annotation for each sample (columns). |
| `KEGG_Pathway_abundance.tsv`                         | Quantification table storing for each KEGG_Pathway (rows) the sum of aligned reads into all genes <br /> having this functional annotation for each sample (columns). |
| `KEGG_Module_abundance.tsv`                          | Quantification table storing for each KEGG_Module (rows) the sum of aligned reads into all genes <br /> having this functional annotation for each sample (columns). |
| `PFAM_abundance.tsv`                                 | Quantification table storing for each PFAM (rows) the sum of aligned reads into all genes having <br /> this functional annotation for each sample (columns). |

### G. 07_taxo_affi

#### 1. 07_1_affiliation_per_sample

| File      | Description                             |
| ----------------------- | --------------------------------------- |
| `SAMPLE_NAME/SAMPLE_NAME.pergene.tsv`                                     | Taxonomic affiliation of genes. One line corresponds to a gene (1st column), its corresponding taxon id (2nd column), <br /> its corresponding lineage (3rd column) and the tax ids of each level of this lineage (4th column). |
| `SAMPLE_NAME/SAMPLE_NAME.warn.tsv`                                        | List of genes with a hit without corresponding taxonomic affiliation. Each line corresponds to a gene (1st column), <br /> the reason why the gene is in this list (2nd column) and match ids into the database used during `05_alignment/05_2_database_alignment/` (3rd column). |
| `SAMPLE_NAME/top_taxons_per_contig.tsv`                                   | Possible affiliation taxons at each rank and for each contig (one line per contig). The score associated with the taxon is indicated in parenthesis. <br /> This file can be helpful to investigate contig affiliation. |
| `SAMPLE_NAME/SAMPLE_NAME.percontig.tsv`                                   | Taxonomic affiliation of contigs. One line corresponds to a contig (1st column), its corresponding taxon id (2nd column), <br /> its corresponding lineage (3rd column) and the tax ids of each level of this lineage (4th column). |
| `SAMPLE_NAME/SAMPLE_NAME_quantif_percontig.tsv`                           | Quantification table of reads aligned on contigs affiliated to each lineage of the first column. One line = one taxonomic affiliation (1st column, `lineage_by_level`), <br /> the corresponding taxon id (2nd column, `consensus_tax_id`), the tax ids of each level of this taxonomic affiliation (3rd column, `tax_id_by_level`), <br /> the name of contigs affiliated to this lineage (4th column, `name_contigs`), the number of contigs affiliated to this lineage (5th column, `nb_contigs`), <br /> the sum of the number of reads aligned to these contigs (6th column, `nb_reads`) and the mean depth of these contigs (6th column, `depth`). |
| `SAMPLE_NAME/SAMPLE_NAME_quantif_percontig_by_[taxonomic_level].tsv`      | One file by taxonomic level (superkingdom, phylum, order, class, family, genus, species) for the sample `SAMPLE_NAME`. <br /> Quantification table of reads aligned on contigs affiliated to each lineage of the corresponding [taxonomic level]. One line = one taxonomic affiliation at this [taxonomic level] <br /> with is taxon id (1st column, `tax_id_by_level`), its lineage (2nd column, `lineage_by_level`), <br /> the name of contigs affiliated to this lineage (3rd column, `name_contigs`), <br /> the number of contigs affiliated to this lineage (4th column, `nb_contigs`), the sum of the number of reads aligned to these contigs (5th column, `nb_reads`) <br /> and the mean depth of these contigs (6th column, `depth`). |
| `SAMPLE_NAME/graphs/SAMPLE_NAME_aln_diamond.m8_contig_taxonomy_level.pdf` | Figure representing the number of contigs (y-axis) affiliated to each taxonomy levels (x-axis). |
| `SAMPLE_NAME/top_taxons_per_contig.tsv`                                   | Details of top possible taxons per contig and for each taxonomic rank. For each rank, there is a list of the top possible taxons and their weigth in parentesis <br /> (only taxons with a weigth > 1 is written). For example in the genus column we can have `Escherichia (68.0);Shigella (20.0);Enterobacter (4.0);Salmonella (4.0);Felsduovirus (4.0)`. This file can be useful to understand an affiliation in detail.  |


#### 2. 07_2_affiliation_merged

| File      | Description                             |
| ----------------------- | --------------------------------------- |
| `quantification_by_contig_lineage_all.tsv`               | Quantification table of reads aligned on contigs affiliated to each lineage. One line = one taxonomic affiliation with its lineage (1st column, `lineage_by_level`), <br /> the taxon id at each level of this lineage (2nd column, `tax_id_by_level`), and then all next 3-columns blocks correspond to one sample. <br /> Each 3-column block corresponds to the name of contigs affiliated to this lineage (1st column, `name_contigs_SAMPLE_NAME_quantif_percontig`), <br /> the number of contigs affiliated to this lineage (2nd column, `nb_contigs_SAMPLE_NAME_quantif_percontig`), <br /> the sum of the number of reads aligned to these contigs (3rd column, `nb_reads_SAMPLE_NAME_quantif_percontig`) <br /> and the mean depth of these contigs (4th column, `depth_SAMPLE_NAME_quantif_percontig`). |
| `quantification_by_contig_lineage_[taxonomic_level].tsv` | One file by taxonomic level (superkingdom, phylum, order, class, family, genus, species). Quantification table of reads aligned on contigs affiliated to each lineage of the corresponding [taxonomic level]. <br /> One line = one taxonomic affiliation at this [taxonomic level] with its taxon id (1st column, `tax_id_by_level`), <br /> its lineage (2nd column, `lineage_by_level`), <br /> and then all next 3-columns blocks correspond to one sample. Each 3-column block corresponds to the name of contigs affiliated to this lineage <br /> (1st column, `name_contigs_SAMPLE_NAME_quantif_percontig_by_[taxonomic_level]`), the number of contigs affiliated to this lineage (2nd column, `nb_contigs_SAMPLE_NAME_quantif_percontig_by_[taxonomic_level]`) and the sum of the number of reads aligned to these contigs <br /> (3rd column, `nb_reads_SAMPLE_NAME_quantif_percontig_by_[taxonomic_level]`) and the mean depth of these contigs (4th column, `depth_SAMPLE_NAME_quantif_percontig_by_[taxonomic_level]`). |

#### 3. 07_3_plot

| File      | Description                             |
| ----------------------- | --------------------------------------- |
| `krona_mean_depth_abundance.html` | Krona plot of the taxonomic affiliation of contigs. <br /> The abundance of a taxon is the percentage of the depth of the contigs affiliated to this taxon.  |
| `krona_read_count_abundance.html` | Krona plot of the taxonomic affiliation of contigs. <br /> The abundance of a taxon is the percentage of reads mapping the contigs affiliated to this taxon. |
| `abundance_per_rank.html`         | This plot represents the abundance of affiliation made for each taxonomic rank.|
| `most_abundant_taxa.html`         | This plot represents the abundance of the 10 most abundant taxa at each taxonomic rank and for each sample.|

### H. 08_binning

| File      | Description                             |
| ----------------------- | --------------------------------------- |
| `genomes_abundances.tsv` | Global informations about the final set of bins. One line = one bin id (1st column, `genome_id`), <br /> its bin name, retrieve from the lowest taxonomic rank affiliated by GTDB-Tk (2nd column, `genome_name`), the Domain, Phylum, Class, Order, Family, Genus, Species <br /> taxonomic affiliations made with GTDB-Tk (3rd, 4th, 5th, 6th, 7th, 8th and 9th columns), its quality metrics : `completeness` and `contamination`, <br /> retrieved with Checkm2 (10th and 11th columns), its size in bp (12th column, `genome_length`), <br /> its N50 metric (13th column, genome_N50), its number of contigs (14th column, `contig_count`). <br /> After that, there is 2 columns per sample : the number of reads and the mean depth associated to the bin within the specicic sample. <br /> Finally, the second to last column (`sum_numreads`) described the bin's total number of reads between all the samples, <br /> and the last (`mean_depth`) the average depth between all sample.|


#### 1. 08_1_binning_per_sample

| File or directory             | Description                             |
| ----------------------- | --------------------------------------- |
| `SAMPLE_NAME/concoct`                              | This directory contains bins fasta files from the individual binning tool CONCOCT (see informations [here](https://concoct.readthedocs.io/en/latest/)). |
| `SAMPLE_NAME/maxbin2`                              | This directory contains bins fasta files from the individual binning tool MaxBin2(see informations [here](https://academic.oup.com/bioinformatics/article/32/4/605/1744462)). |
| `SAMPLE_NAME/metabat2`                             | This directory contains bins fasta files from the individual binning tool MetaBAT2                     (see informations [here](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC6662567/)). |
| `SAMPLE_NAME/bin_refinement`                       | This directory contains bins fasta files and statistics from the aggregate binning tool BINETTE, <br /> which takes into account the bins sets from the 3 individual binning tools to try to improve the quality of the bins (see informations [here](https://github.com/genotoul-bioinfo/Binette)). |
| `SAMPLE_NAME/bin_refinement/unbinned_contigs.fasta`| Contigs from the sample assembly that are not retrieved in any bin. The proportion of non-binned contigs to the global size of the sample assembly can be seen in the MultiQC report, <br /> in the section Bins Size (bp) quality. |

#### 2. 08_2_dereplicated_bins

| File or directory             | Description                             |
| ----------------------- | --------------------------------------- |
| `dereplicated_genomes/`                     | The dRep software allows to compare the bins of all samples in a pair-wise manner, <br /> and to clusterize the bins that share similar DNA content in terms of Average Nucleotide Identity (ANI). <br /> The threshold of 95% ANI is used to create a set of species-level <br /> representative genomes (SRGs). <br /> Within each cluster of bins, the best bin in terms of quality (completeness,contamination,N50,strain-heterogeneity) <br /> is selected as representative genome. <br /> The directory dereplicated_genomes contains all the representative bins that will be used in order to compare abundances of SRGs between the samples.  |
| `data_tables/genomeInformation.csv`         | Calculated metrics of all the bins before de-replication, used to select the representative bin after de-replication. <br /> Completeness and contamination where calculated before with CheckM2, during bin_refinement step. <br /> dRep adds further metrics and use all four to choose the best bin. |
| `data_tables/Bins_clusters_composition.tsv` | Associates each representative bins cluster (1st column) to the bins list within the cluster (2nd column, bins seperates by commas). |
| `figures/ `                                 | `Primary_clustering_dendrogram.pdf`: The primary clustering dendrogram summarizes the pair-wise Mash distance between all genomes in the genome list. <br /> The first clusturing used Mash, an incredibly fast but not robust algorithm, in order to accelerate the process during the second clusturing. <br /> `Secondary_clustering_dendrograms.pdf`: Each primary cluster with more than one member will have a page in the Secondary clustering dendrograms file. <br /> Bins with more than **95% of ANI distance similarity** <br /> (--drep_threshold default value paramater, that correspond to the threshold for separate species.) will be grouped together. <br /> **You can try different dRep thresholds to be more or less stringent, for example if the goal of dereplication is to generate a set of genomes that are distinct when mapping short reads, 98% ANI is an appropriate threshold.** <br /> You can also try higher threshold in order to separate species that belong to low divergent clades (96, or 97%). <br /> `Cluster_scoring.pdf`: Each secondary cluster will have its own page in the Cluster scoring figure. These figures show the score of each genome, as well as all metrics that can go into determin, and will always be the genome with the highest score.`Clustering scatterplots.pdf` provides some information about genome alignment statistics, and <br /> `Winning genomes.pdf` provides some information about only the “best” genomes of each replicate set, as well as a couple quick overall statistics.|

If you want to make further analysis about intra-population genetic diversity (microdiversity) on the genomes, you can use **inStrain** software specifically developed for this purpose. The documentation is [here](https://instrain.readthedocs.io/en/latest/tutorial.html#quick-start).

 metagWGS outputs allows to easily run inStrain and are available here :

 - bam files : results/08_binning/08_4_mapping_on_final_bins/mapping/first_sample/first_sample.sort.bam

 - bin fasta files : results/08_binning/08_2_dereplicated_bins/dereplicated_genomes/first_bin.fa


#### 3. 08_3_gtdbtk

| File      | Description                                           |
| ----------------------- | --------------------------------------- |
| `gtdbtk.bac120.summary.tsv` | Taxonomic classifications provided by GTDB-Tk. One line = one bin id (1st column, `user_genome`), <br /> its taxonomical classification based on the closest reference genome from the GTDB-Tk database (2nd column, `classification`), <br /> the accession number of the closest reference genome (3rd column, `fastani_reference`). <br /> Please see GTDB-Tk documentation [here](https://ecogenomics.github.io/GTDBTk/files/summary.tsv.html) for information on additional columns. |

#### 4. 08_4_mapping_on_final_bins

| File      | Description                                           |
| ----------------------- | --------------------------------------- |
| `mapping/SAMPLE_NAME/`                         | In order to compare genomes abundances between samples, <br /> mapping of metagenomics samples reads against the final set of de-replicated bins is performed. |
| `mapping/SAMPLE_NAME/SAMPLE_NAME.sort.bam`     | Alignment of reads on contigs (.bam file). |
| `mapping/SAMPLE_NAME/SAMPLE_NAME.sort.bam.bai` | Index of .bam file. |

### I. MultiQC

| File                    | Description                             |
| ----------------------- | --------------------------------------- |
| `multiqc_report.html`   | MultiQC report file containing graphs and a summary of analysis done by metagWGS. <br /> You will find
explanations of the graphs in the multiQC report on this [page](https://bios4biol.pages.mia.inra.fr/Help4MultiQC/). |

### J. pipeline_info

| File                    | Description                             |
| ----------------------- | --------------------------------------- |
| `software_versions.csv` | Indicates the versions of the tools used in the pipeline.     |
| `db_versions.tsv`       | Indicates the size, date of last modification and path of the file or folder for each databank used in the pipeline. <br /> For the host genome, the number of sequence is in parenthesis with the size of the file.  |

## III. Description of other files in your working directory (not in `results` directory):

| File          | Description            |
| ------------- | ---------------------- |
| `.nextflow_log` | Log file from Nextflow. |

## IV. Other files can be added to the working directory (not in `results` directory) if you use Nextflow specific options:

| Option        | File            | Description            |
| ------------- | --------------- | ---------------------- |
| `-with-trace` | `trace.txt` | Execution tracing file of the pipeline, containing informations as location of cache directories and memory and cpus statistics for each job. <br /> More explanations [here](https://www.nextflow.io/docs/latest/tracing.html?highlight=dag#trace-report). |
| `-with-report ` | `report.html` | Execution metrics of workflow execution. Contains metadata with shell commands launched by each process. <br /> More explanations [here](https://www.nextflow.io/docs/latest/tracing.html?highlight=dag#execution-report). |
| `-with-timeline` | `timeline.html` | Time metrics of workflow execution. More explanations [here](https://www.nextflow.io/docs/latest/tracing.html?highlight=dag#timeline-report). |
| `-with-dag` | `dag.dot` | Modelisation of the pipeline by a Direct Acyclic Graph. More explanations [here](https://www.nextflow.io/docs/latest/tracing.html?highlight=dag#dag-visualisation). |
