.. metagwgs documentation master file, created by
   sphinx-quickstart on Wed Sep 25 14:51:01 2024.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

metagWGS documentation
======================

metagWGS, a comprehensive workflow to analyze metagenomic data using Illumina or PacBio HiFi reads.

.. Add your content using ``reStructuredText`` syntax. See the
   `reStructuredText <https://www.sphinx-doc.org/en/master/usage/restructuredtext/index.html>`_
   documentation for details.

.. figure:: images/metagwgs_metro_map.png
   :class: with-border
   :align: center


.. toctree::
   :maxdepth: 2
   :glob:
   :hidden:
   :caption: Contents:

   installation
   usage
   output
   use_case
   functionnal_tests

