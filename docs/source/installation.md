# metagWGS: installation procedure

## I. Install Nextflow

metagWGS is a [Nextflow](https://www.nextflow.io/) pipeline, you must install Nextflow to run it.
Nextflow runs on most POSIX systems (Linux, Mac OSX etc). 
It can be installed by following the instructions [here](https://www.nextflow.io/docs/latest/getstarted.html#installation). 

```{warning}
Change your `$PATH` variable to run Nextflow in any directory.
```

For any issue during Nextflow installation and use, you can see [this tutorial](https://genotoul-bioinfo.pages.mia.inra.fr/use-nextflow-nfcore-course/pages/nextflow_usage/) and [this documentation](https://www.nextflow.io/docs/latest/index.html).

```{note}
If you are on [genobioinfo cluster](http://bioinfo.genotoul.fr/), you don't need to install Nextflow.
```

You only need to know Nextflow version installed on genobioinfo.
* Run `search_module Nextflow` to find Nextflow available versions.
* See corresponding help [here](https://genotoul-bioinfo.pages.mia.inra.fr/use-nextflow-nfcore-course/pages/nextflow_usage/).
* Keep in mind the last version of Nextflow installed (>=v20).

```{warning}
You must use Nextflow versions available [here](http://bioinfo.genotoul.fr/index.php/how-to-use/?software=How_to_use_SLURM_Nextflow).
Don't use `bioinfo/nfcore-Nextflow` versions of Nextflow.
```

## II. Install metagWGS

**In the directory you want to run the workflow**, launch:

```bash
git clone https://forgemia.inra.fr/genotoul-bioinfo/metagwgs.git
```

A directory called `metagwgs` containing all source files of the pipeline have been downloaded.

## III. Install Singularity

metagWGS needs two [Singularity](https://sylabs.io/docs/) containers to run: Singularity version 3.4.0 or above must be installed.

See [here](https://sylabs.io/guides/3.7/user-guide/quick_start.html#quick-installation-steps) how to install Singularity >=v3.4.0.

```{note}
If you are on [genobioinfo cluster](http://bioinfo.genotoul.fr/), you can run `search_module Singularity` to find Singularity available versions.
Then, run `module load Singularity_module` of the last version (`Singularity_module`) of Singularity installed (>=v3.4.0).
```

```{warning}
For any issue during Singularity use on genobioinfo cluster,
you can see [this tutorial](https://genotoul-bioinfo.pages.mia.inra.fr/use-nextflow-nfcore-course/pages/login_genotoul/#load-modules).
```

## IV. Download or build Singularity containers

You can directly download the two Singularity containers (`Solution 1`, recommended) or build them (`Solution 2`).

### Solution 1 (recommended): download the two containers

**In the directory you want to run the workflow**, where you have the directory `metagwgs` with metagWGS source files, run these command lines:

```bash
cd metagwgs/env/
singularity pull metagwgs.sif oras://registry.forgemia.inra.fr/genotoul-bioinfo/metagwgs/metagwgs:v2.4.3
singularity pull binning.sif oras://registry.forgemia.inra.fr/genotoul-bioinfo/metagwgs/binning:v2.4.3
```

Two files (`metagwgs.sif` and `binning.sif`) must have been downloaded.

```{note}
If you are using the devel branch, you must use :

```bash
singularity pull binning.sif oras://registry.forgemia.inra.fr/genotoul-bioinfo/metagwgs/binning_devel:latest
singularity pull metagwgs.sif oras://registry.forgemia.inra.fr/genotoul-bioinfo/metagwgs/metagwgs_devel:latest
```

### Solution 2: build the two containers.

**In the directory you want to run the workflow**, where you have downloaded metagWGS source files, go to `metagwgs/env/` directory, and follow [these explanations](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/wikis/Singularity-container) to build the two containers. You need two files by container to build them. These files are into the `metagwgs/env/` folder and you can read them here:

* metagwgs.sif container
   * [metagWGS recipe file](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/env/Singularity_recipe_metagWGS?ref_type=heads)
   * [metagWGS.yml](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/env/metagWGS.yml?ref_type=heads)
* binning.sif container
   * [binning recipe file](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/env/Singularity_recipe_binning?ref_type=heads)
   * [binning.yml](https://forgemia.inra.fr/genotoul-bioinfo/metagwgs/-/blob/master/env/binning.yml?ref_type=heads)

At the end of the build, two files (`metagwgs.sif` and `binning.sif`) must have been generated.
