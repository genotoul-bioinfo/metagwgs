# Configuration file for the Sphinx documentation builder.
#
# For the full list of built-in configuration values, see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Project information -----------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#project-information

import os
import sys
sys.path.insert(0, os.path.abspath('../'))


project = 'metagWGS'
copyright = '2024, Joanna Fourquet, Jean Mainguy, Maïna Vienne, Céline Noirot, Pierre Martin, et al..'
author = 'Joanna Fourquet, Jean Mainguy, Maïna Vienne, Céline Noirot, Pierre Martin, et al..'
release = 'v2.4.3'

# -- General configuration ---------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#general-configuration

extensions = [
    "sphinx.ext.viewcode",
    "sphinx.ext.autodoc",
    "sphinx_rtd_theme",
    "myst_parser",
    "sphinx.ext.mathjax",
    "sphinx_multiversion",
    "sphinx.ext.autosectionlabel",
    "sphinx.ext.intersphinx",
    "sphinx_copybutton",
]

source_suffix = {
    ".rst": "restructuredtext",
    ".txt": "markdown",
    ".md": "markdown",
}

templates_path = ['_templates']
html_sidebars = {
    '**': [
        'versioning.html',
        'searchbox.html',
        'navigation.html',
    ]
}
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------
# https://www.sphinx-doc.org/en/master/usage/configuration.html#options-for-html-output

html_theme = 'sphinx_rtd_theme'
html_static_path = ['_static']

# for sphinx-multiversions # https://github.com/Holzhaus/sphinx-multiversion 
# https://blog.stephane-robert.info/post/sphinx-documentation-multi-version/ 
smv_tag_whitelist = r'^v\d+\.\d+\.\d+$'  # tags of form v*.*.x and latest 
# Whitelist pattern for branches (set to '' to ignore all branches) 
smv_branch_whitelist = r'^(master|devel)$' 
smv_remote_whitelist = r'^origin$'
smv_outputdir_format = '{ref.name}'
smv_prefer_remote_refs = True