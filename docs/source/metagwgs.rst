metagwgs package
===================
Submodules
----------
metagwgs.metagwgs module
------------------------------
.. automodule:: metagwgs.metagwgs
   :members:
   :undoc-members:
   :show-inheritance:
Module contents
---------------
.. automodule:: metagwgs
   :members:
   :undoc-members:
   :show-inheritance:
